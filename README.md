# react-native-xmpp

React native xmpp client

## Installation

```sh
npm install react-native-xmpp
```

## Usage

```js
import { multiply } from "react-native-xmpp";

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
